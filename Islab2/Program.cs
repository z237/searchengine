﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Islab2 {
	class Stemmer {
		public Stemmer(string w) {
			Word = w;
		}

		//	word to be stemmed
		public string Word;

		//	is the letter consonant
		private	bool IsConsonant(int i) {
			char c = Word[i];
			switch (c) {
				case 'a': case 'e': case 'i': case 'o': case 'u': return false;
				case 'y': return (i == 0) ? true : !IsConsonant(i - 1);
				default: return true; 
			}
		}

		//	measures number of consonant sequences
		private int M() {
			int n = 0;
			int i = 0;
			int j = Word.Length - 1;
			while (true) {
				if (i > j) return n;
				if (!IsConsonant(i)) break; i++;
			}
			i++;
			while (true) {
				while (true) {
					if (i > j) return n;
					if (IsConsonant(i)) break;
					i++;
				}
				i++;
				n++;
				while (true) {
					if (i > j) return n;
					if (!IsConsonant(i)) break;
					i++;
				}
				i++;
			}
		}

		//	counts vowels
		private int VowelCount() {
			int count = 0;
			for (int i = 0; i < Word.Length; i++)
				count += IsConsonant(i) ? 0 : 1;
			return count;
		}

		//	Checks whether word ends with variable or not
		private bool EndsWith(string ending) {
			if (Word.Length < ending.Length)
				return false;
			return Word.LastIndexOf(ending) == Word.Length - ending.Length;
		}

		//	Checks whether word ends with double consentant or not
		private bool IsDoubleC() {
			int last = Word.Length - 1;
			return Word[last] == Word[last - 1] && IsConsonant(last);
		}

		//	Checks whether word ends with cvc or not
		private bool IsCVC() { 
			int last = Word.Length - 1;
			if (last < 2)
				return false;
			return IsConsonant(last) && !IsConsonant(last - 1) && IsConsonant(last - 2);
		}

		//	Remove n symbols from the end
		private void RmnFromEnd(int i) {
			Word = Word.Remove(Word.Length - i);
		}

		//	Apply first step of stemming
		private void Step1() {
			//	remove plural
			if (EndsWith("sses")) {
				RmnFromEnd(2);
			} else if (EndsWith("ies")) {
				RmnFromEnd(2);
			} else if (!EndsWith("ss") && EndsWith("s")) {
				RmnFromEnd(1);
			}
			if (EndsWith("\'"))
				RmnFromEnd(1);

			//	remove endings
			if (EndsWith("eed")) {
				if (M() > 1)
					RmnFromEnd(1);
			} else if (VowelCount() > 1 && (EndsWith("ed") || EndsWith("ing"))) {
				if (EndsWith("ed"))
					RmnFromEnd(2);
				else if (EndsWith("ing"))
					RmnFromEnd(3);

				if (EndsWith("at") || EndsWith("bl") || EndsWith("iz"))
					Word += "e";

				if (IsDoubleC()) {
					if (!EndsWith("z") && !EndsWith("l") && !EndsWith("s"))
						RmnFromEnd(1);
				} else if (IsCVC() && M() == 1) {
					Word += "e";
				}
			}

			//	Change y to i
			if (EndsWith("y") && VowelCount() > 1) {
				RmnFromEnd(1);
				Word += 'i';
			}
		}

		//	Apply second step of stemming
		private void Step2() {
			string tmp = Word;
			if (EndsWith("ational")) {
				RmnFromEnd(7);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ate";
			} else if (EndsWith("tional")) {
				RmnFromEnd(6);
				if (M() == 0)
					Word = tmp;
				else
					Word += "tion";
			} else if (EndsWith("enci")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ence";
			} else if (EndsWith("anci")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ance";
			} else if (EndsWith("izer")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ize";
			} else if (EndsWith("abli")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "able";
			} else if (EndsWith("alli")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "al";
			} else if (EndsWith("entli")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ent";
			} else if (EndsWith("eli")) {
				RmnFromEnd(3);
				if (M() == 0)
					Word = tmp;
				else
					Word += "e";
			} else if (EndsWith("ousli")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ous";
			} else if (EndsWith("ization")) {
				RmnFromEnd(7);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ize";
			} else if (EndsWith("ation")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ate";
			} else if (EndsWith("ator")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ate";
			} else if (EndsWith("alism")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "al";
			} else if (EndsWith("iveness")) {
				RmnFromEnd(7);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ive";
			} else if (EndsWith("fulness")) {
				RmnFromEnd(7);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ful";
			} else if (EndsWith("ousness")) {
				RmnFromEnd(7);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ous";
			} else if (EndsWith("aliti")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "al";
			} else if (EndsWith("iviti")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ive";
			} else if (EndsWith("biliti")) {
				RmnFromEnd(6);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ble";
			}
		}

		//	Apply third step of stemming
		private void Step3() {
			string tmp = Word;
			if (EndsWith("icate")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ic";
			} else if (EndsWith("ative")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
			} else if (EndsWith("alize")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "al";
			} else if (EndsWith("iciti")) {
				RmnFromEnd(5);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ic";
			} else if (EndsWith("ical")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
				else
					Word += "ic";
			} else if (EndsWith("ful")) {
				RmnFromEnd(3);
				if (M() == 0)
					Word = tmp;
			} else if (EndsWith("ness")) {
				RmnFromEnd(4);
				if (M() == 0)
					Word = tmp;
			}
		}

		//	Apply fourth step of stemming
		private void Step4() {
			string tmp = Word;
			if (EndsWith("al")) {
				RmnFromEnd(2);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ance")) {
				RmnFromEnd(4);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ence")) {
				RmnFromEnd(4);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("er")) {
				RmnFromEnd(2);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ic")) {
				RmnFromEnd(2);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("able")) {
				RmnFromEnd(4);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ible")) {
				RmnFromEnd(4);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ant")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ement")) {
				RmnFromEnd(5);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ment")) {
				RmnFromEnd(4);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ent")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ion")) {
				RmnFromEnd(3);
				if (M() <= 1 && (Word[Word.Length - 1] == 't' ||
												 Word[Word.Length - 1] == 's'))
					Word = tmp;
			} else if (EndsWith("ou")) {
				RmnFromEnd(2);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ism")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ate")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("iti")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ous")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ive")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			} else if (EndsWith("ize")) {
				RmnFromEnd(3);
				if (M() <= 1)
					Word = tmp;
			}
		}

		//	Apply fifth step of stemming
		private void Step5() {
			string tmp = Word;
			if (EndsWith("e")) {
				RmnFromEnd(1);
				if (M() <= 1) {
					if (M() == 1) {
						if (IsCVC())
							Word += 'e';
					}
				}
			}

			if (M() > 1 && IsDoubleC() && Word[Word.Length - 1] == 'l')
				RmnFromEnd(1);
		}

		//	Apply sixth step of stemming 
		private void Step6() {

		}

		public string GetStem() {
			Step1();
			Step2();
			Step3();
			Step4();
			Step5();
			Step6();
			return Word;
		}
	}
  class Program {
		static void Main(string[] args) {
			//	Get stop words from file
			string[] SW = File.ReadAllLines("stopWords.txt");
			HashSet<string> stopWords = new HashSet<string>(SW);
			//	CHANGE
			string PathToFiles = "dictionary";
			string[] AllFiles = Directory.EnumerateFiles(PathToFiles).ToArray();
			int FilesCount = AllFiles.Length;
			char[] delimiters = { ' ', ',', '.', ':', '\t', '?',
														'!', '~', '\n', '(', ')', ';',
														'-', '–', '—', '\r', '"'};

			Stemmer S = new Stemmer("");
			Dictionary<string, int>[] Frequency = 
				new Dictionary<string, int>[FilesCount];
			Dictionary<string, double>[] TermFrequency = 
				new Dictionary<string, double>[FilesCount];

			//	Calculate frequency in files
			for (int i = 0; i < FilesCount; i++) {
				Frequency[i] = new Dictionary<string, int>();
				string[] firstFile = File
															.ReadAllText(AllFiles[i])
															.Split(delimiters);
				foreach (string w in firstFile) {
					string s = w.ToLower();
					if (!stopWords.Contains(s)) {
						if (s == "")
							continue;
						S.Word = s;
						S.GetStem();
						s = S.Word;
						//Console.WriteLine(s);
						int x;
						Frequency[i].TryGetValue(s, out x);
						if (x == 0)
							Frequency[i].Add(s, 0);
						Frequency[i][s]++;
					}
				}

				TermFrequency[i] = new Dictionary<string, double>();
				foreach (KeyValuePair<string, int> p in Frequency[i])
					TermFrequency[i].Add(p.Key, p.Value * 1.0 / firstFile.Length);
			}

			Dictionary<string, double> IDF = new Dictionary<string, double>();

			//	Working with query
			while (true) {
				string[] query = Console
													.ReadLine()
													.Split(delimiters);

				//	Stemming
				for (int i = 0; i < query.Length; i++) {
					S.Word = query[i];
					S.GetStem();
					query[i] = S.Word;
				}

				Dictionary<string, double> IDF_TF = new Dictionary<string, double>();
				for (int i = 0; i < FilesCount; i++)
					IDF_TF.Add(AllFiles[i], 0);

				//	Calculate Summ IDF * TF for each file
				for (int i = 0; i < query.Length; i++) {
					string word = query[i];
					double x;
					IDF.TryGetValue(word, out x);
					//	If IDF not calculated yet
					if (x == 0) {
						foreach(var d in TermFrequency) {
							if (d.ContainsKey(word))
								x++;
						}
						if (x != 0) {
							double expr = Math.Log10(FilesCount * 1.0 / x);
							if (expr != 0)
								IDF.Add(word, expr);
							else
								IDF.Add(word, -1);
						} else
							IDF.Add(word, -1);
					}

					//	If word not present in any file
					if (IDF[word] == -1)
						continue;

					for (int f = 0; f < FilesCount; f++) {
						double tf;
						TermFrequency[f].TryGetValue(word, out tf);
							IDF_TF[AllFiles[f]] += tf * IDF[word];
					}
				}

				List<KeyValuePair<string, double>> SortDict = IDF_TF.ToList();
				SortDict.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));

				foreach (var x in SortDict)
					if (x.Value != 0)
						Console.WriteLine(x.Key + " - " + x.Value);
				Console.WriteLine();
				//for (int i = 0; i < FilesCount; i++)
				//	Console.WriteLine(IDF_TF[AllFiles[i]] + " - " + AllFiles[i]);
			}
		}
	}
}
